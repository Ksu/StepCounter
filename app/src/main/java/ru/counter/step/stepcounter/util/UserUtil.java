package ru.counter.step.stepcounter.util;

public class UserUtil {

    private static final String USER_ID = "user_id";
    private static final int TEST_USER_ID = 543;

    public static void setUserId(int id) {
        PrefUtils.getEditor().putInt(USER_ID, id).commit();
    }

    public static int getUserId() {
        return PrefUtils.getPrefs().getInt(USER_ID, TEST_USER_ID);
    }
}
