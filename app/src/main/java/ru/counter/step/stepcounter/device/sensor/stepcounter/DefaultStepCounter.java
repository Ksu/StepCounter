package ru.counter.step.stepcounter.device.sensor.stepcounter;

import android.hardware.Sensor;
import android.hardware.SensorManager;

import javax.annotation.Nonnull;

import ru.counter.step.stepcounter.util.RxSensorUtil;
import ru.counter.step.stepcounter.util.Utils;
import rx.Subscription;

import static android.hardware.Sensor.TYPE_STEP_COUNTER;

public class DefaultStepCounter implements StepCounter {

    public static final int SENSOR_TYPE = TYPE_STEP_COUNTER;
    private static final int BATCH_LATENCY = 0; // max batch latency, microseconds. 0 = no batching
    private static final int SENSOR_DELAY = SensorManager.SENSOR_DELAY_FASTEST; // sampling period, microseconds

    @Nonnull
    private SensorManager sensorManager;
    @Nonnull
    private Sensor sensor;
    private StepListener stepListener;
    private Subscription counterSubscription;

    private int startCounterSteps;
    public int totalTrackSteps;

    public DefaultStepCounter(@Nonnull SensorManager sensorManager, @Nonnull Sensor sensor) {
        this.sensorManager = sensorManager;
        this.sensor = sensor;
    }

    public void registerStepListener(StepListener listener) {
        stepListener = listener;
        totalTrackSteps = 0;
        startCounterSteps = 0;

        counterSubscription = RxSensorUtil.observeSensorChanged(sensorManager, sensor, SENSOR_DELAY, BATCH_LATENCY)
                .compose(Utils.applySchedulers())
                .subscribe(event -> {
                    if (startCounterSteps < 1) {
                        startCounterSteps = (int) event.values[0];
                    }
                    totalTrackSteps = (int) event.values[0] - startCounterSteps;

                    if (stepListener != null) {
                        stepListener.onStepsDetected(totalTrackSteps);
                    }
                }, exception -> {
                    exception.printStackTrace();
                    if (stepListener != null) {
                        stepListener.onError("Ошибка получения данных");
                    }
                });
    }

    @Override
    public void unregisterStepListener() {
        stepListener = null;
        counterSubscription.unsubscribe();
        totalTrackSteps = 0;
    }
}
