package ru.counter.step.stepcounter.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import ru.counter.step.stepcounter.presentation.model.Track;

@Dao
public interface TrackDao {

    @Insert
    long insertTrack(Track track);

    @Query("SELECT * FROM track WHERE userId = :userId")
    Single<List<Track>> getTracksByUserId(long userId);

    @Query("SELECT * FROM track WHERE userId = :userId AND id = (SELECT MAX(id) FROM track)")
    Single<Track> getLastTrackByUserId(long userId);

    @Query("UPDATE track SET totalTrackSteps = :totalTrackSteps, totalTrackTimeSec = :totalTrackTime WHERE id =:trackId")
    void updateTrack(long trackId, long totalTrackSteps, long totalTrackTime);

    @Query("DELETE FROM track")
    void deleteAll();
}