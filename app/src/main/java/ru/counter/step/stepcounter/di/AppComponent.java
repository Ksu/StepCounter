package ru.counter.step.stepcounter.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.notification.NotificationFactory;
import ru.counter.step.stepcounter.device.sensor.TrackManager;
import ru.counter.step.stepcounter.device.sensor.stepcounter.LegacyStepCounter;
import ru.counter.step.stepcounter.device.sensor.stepcounter.StepCounterManager;
import ru.counter.step.stepcounter.device.sensor.StepCounterService;
import ru.counter.step.stepcounter.di.module.AppModule;
import ru.counter.step.stepcounter.di.module.ContextModule;
import ru.counter.step.stepcounter.di.module.DeviceModule;
import ru.counter.step.stepcounter.di.module.RepositoryModule;
import ru.counter.step.stepcounter.presentation.presenter.CounterPresenter;
import ru.counter.step.stepcounter.presentation.presenter.SettingsPresenter;

@Singleton
@Component(modules = {
        ContextModule.class,
        AppModule.class,
        RepositoryModule.class,
        DeviceModule.class})
public interface AppComponent {

    void inject(StepCounterApp SCApp);

    Context getContext();

    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();

        Builder contextModule(ContextModule contextModule);
    }

    void inject(CounterPresenter counterPresenter);
    void inject(SettingsPresenter settingsPresenter);
    void inject(StepCounterManager stepCounterManager);
    void inject(StepCounterService stepCounterService);
    void inject(LegacyStepCounter legacyStepCounter);
    void inject(NotificationFactory notificationFactory);
    void inject(TrackManager trackManager);
}