package ru.counter.step.stepcounter.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.counter.step.stepcounter.db.MilestoneDao;
import ru.counter.step.stepcounter.db.StepCounterDB;
import ru.counter.step.stepcounter.db.TrackDao;

@Module
public class AppModule {

    @Provides
    @Singleton
    StepCounterDB providesDatabase(Application app) {
        return Room.databaseBuilder(app, StepCounterDB.class, "step_counter_db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    MilestoneDao provideMilestoneDao(StepCounterDB db) {
        return db.milestoneDao();
    }

    @Singleton
    @Provides
    TrackDao provideTrackDao(StepCounterDB db) {
        return db.trackDao();
    }
}
