package ru.counter.step.stepcounter.ui.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.counter.step.stepcounter.R;
import ru.counter.step.stepcounter.presentation.presenter.CounterPresenter;
import ru.counter.step.stepcounter.presentation.view.StepCounterView;
import ru.counter.step.stepcounter.util.FormatUtil;

public class MainActivity extends MvpAppCompatActivity implements StepCounterView {

    @InjectPresenter
    CounterPresenter mCounterPresenter;

    @BindView(R.id.step_history)
    TextView mStepHistoryView;
    @BindView(R.id.current_steps)
    TextView mCurrentStepsView;
    @BindView(R.id.current_timer)
    TextView mCurrentTimerView;
    @BindView(R.id.button_start_track)
    Button mStartButtonView;
    @BindView(R.id.button_resume_track)
    Button mResumeButtonView;
    @BindView(R.id.button_stop_track)
    Button mStopButtonView;
    @BindView(R.id.warning)
    TextView mWarningView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.main_toolbar));
        ButterKnife.bind(this);
        mStepHistoryView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @OnClick(R.id.button_start_track)
    public void startTrack(View view) {
        mCounterPresenter.turnOnCounter();
    }

    @OnClick(R.id.button_resume_track)
    public void resumeTrack(View view) {
        mCounterPresenter.resumeCounter();
    }

    @OnClick(R.id.button_stop_track)
    public void stopTrack(View view) {
        mCounterPresenter.turnOffCounter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_latency:
                mCounterPresenter.changeHistoryLatency();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void addMilestoneRecord(long trackId, long totalTrackSteps, long totalTrackTime) {
        String record = "Обход № " + String.valueOf(trackId) + ": уже " + String.valueOf(totalTrackSteps) + " шагов за " + FormatUtil.getFormattedTimerValue(totalTrackTime);
        mStepHistoryView.append("\n" + record);
    }

    @Override
    public void showCurrentSteps(String currentTrackSteps) {
        mCurrentStepsView.setText(currentTrackSteps);
    }

    @Override
    public void showCurrentTimer(long currentTrackTimeMS) {
        String formattedTime = FormatUtil.getFormattedTimerValue(currentTrackTimeMS);
        mCurrentTimerView.setText(formattedTime);
    }

    @Override
    public void showError(String message) {
        mCurrentStepsView.setText(message);
    }

    @Override
    public void showHistoryLoadingError(String message) {
        mStepHistoryView.append("\n" + message);
    }

    @Override
    public void showWarning(String message) {
        mWarningView.setText(message);
    }

    @Override
    public void counterActive() {
        disableButton(mStartButtonView);
        disableButton(mResumeButtonView);
        enableButton(mStopButtonView);
    }

    @Override
    public void counterInactive() {
        enableButton(mStartButtonView);
        disableButton(mResumeButtonView);
        disableButton(mStopButtonView);
        showCurrentSteps("");
        mCurrentTimerView.setText("");
    }

    @Override
    public void allowResume() {
        enableButton(mResumeButtonView);
    }

    private void enableButton(Button button) {
        button.setEnabled(true);
        button.setTextColor(ContextCompat.getColor(this, R.color.colorEnabledButton));
    }

    private void disableButton(Button button) {
        button.setEnabled(false);
        button.setTextColor(ContextCompat.getColor(this, R.color.colorDisabledButton));
    }
}
