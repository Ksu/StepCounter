package ru.counter.step.stepcounter.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ru.counter.step.stepcounter.presentation.model.Milestone;

@Dao
public interface MilestoneDao {

    @Insert
    void insertMilestone(Milestone milestone);

    @Query("SELECT * FROM (SELECT * FROM milestone WHERE userId = :userId ORDER BY id DESC limit 50) ORDER BY id")
    Single<List<Milestone>> getMilestonesByUserId(long userId);

    @Query("SELECT * FROM milestone WHERE trackId = :trackId")
    Flowable<List<Milestone>> getMilestonesByTrackId(long trackId);

    @Query("DELETE FROM milestone")
    void deleteAll();
}