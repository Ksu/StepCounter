package ru.counter.step.stepcounter.repository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.counter.step.stepcounter.AppExecutors;
import ru.counter.step.stepcounter.db.MilestoneDao;
import ru.counter.step.stepcounter.presentation.model.Milestone;

public class MilestoneRepository {
    private final MilestoneDao milestoneDao;
    private final AppExecutors appExecutors;

    @Inject
    public MilestoneRepository(MilestoneDao milestoneDao, AppExecutors appExecutors) {
        this.milestoneDao = milestoneDao;
        this.appExecutors = appExecutors;
    }

    public void insertMilestone(Milestone milestone) {
        milestoneDao.insertMilestone(milestone);
    }

    public Single<List<Milestone>> getMilestonesByUserId(long userId) {
        return milestoneDao.getMilestonesByUserId(userId);
    }

    public void clear() {
        appExecutors.diskIO().execute(milestoneDao::deleteAll);
    }
}
