package ru.counter.step.stepcounter.repository;

import javax.inject.Inject;

import ru.counter.step.stepcounter.util.HistoryLatencyUtil;

public class AppSettingsRepository {

    @Inject
    public AppSettingsRepository() {
    }

    public void setHistoryLatency(int latency) {
        HistoryLatencyUtil.setHistoryLatency(latency);
    }

    public int getHistoryLatency() {
        return HistoryLatencyUtil.getHistoryLatency();
    }
}
