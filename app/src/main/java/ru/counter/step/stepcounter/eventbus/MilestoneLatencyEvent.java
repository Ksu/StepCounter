package ru.counter.step.stepcounter.eventbus;

public class MilestoneLatencyEvent {

    public final int newLatencySec;

    public MilestoneLatencyEvent(int newLatencySec) {
        this.newLatencySec = newLatencySec;
    }
}
