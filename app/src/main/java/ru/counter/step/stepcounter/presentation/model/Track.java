package ru.counter.step.stepcounter.presentation.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "track")
public class Track {
    @SerializedName("id")
    @NonNull
    @Expose
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @SerializedName("userId")
    @Expose
    @ColumnInfo(name = "userId")
    private long userId;

    @SerializedName("totalTrackSteps")
    @Expose
    @ColumnInfo(name = "totalTrackSteps")
    private long totalTrackSteps;

    @SerializedName("totalTrackTimeSec")
    @Expose
    @ColumnInfo(name = "totalTrackTimeSec")
    private long totalTrackTimeSec;

    public Track(long userId) {
        this.userId = userId;
    }

    @NonNull
    public long getId() {
        return id;
    }

    public void setId(@NonNull long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getTotalTrackSteps() {
        return totalTrackSteps;
    }

    public void setTotalTrackSteps(long totalTrackSteps) {
        this.totalTrackSteps = totalTrackSteps;
    }

    public long getTotalTrackTimeSec() {
        return totalTrackTimeSec;
    }

    public void setTotalTrackTimeSec(long totalTrackTimeSec) {
        this.totalTrackTimeSec = totalTrackTimeSec;
    }
}
