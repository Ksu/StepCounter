package ru.counter.step.stepcounter.device.sensor.stepcounter;

public interface StepListener {
    void onStepsDetected(int totalTrackSteps);

    void onError(String message);
}
