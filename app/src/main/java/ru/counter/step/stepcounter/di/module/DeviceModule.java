package ru.counter.step.stepcounter.di.module;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.counter.step.stepcounter.device.notification.NotificationFactory;
import ru.counter.step.stepcounter.device.sensor.TrackManager;
import ru.counter.step.stepcounter.device.sensor.stepcounter.StepCounterManager;
import ru.counter.step.stepcounter.device.sensor.stepcounter.legacydevice.LegacyStepDetector;

@Module(includes = {ContextModule.class})
public class DeviceModule {

    @Provides
    @Singleton
    PackageManager providePackageManager(Context context) {
        return context.getPackageManager();
    }

    @Provides
    @Singleton
    @Nullable
    SensorManager provideSensorManager(Context context) {
        return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    @Provides
    @Singleton
    TrackManager provideTrackManager() {
        return new TrackManager();
    }

    @Provides
    @Singleton
    StepCounterManager provideStepCounterManager() {
        return new StepCounterManager();
    }

    @Provides
    LegacyStepDetector provideLegacyStepDetector() {
        return new LegacyStepDetector();
    }

    @Provides
    @Singleton
    NotificationFactory provideNotificationFactory() {
        return new NotificationFactory();
    }
}
