package ru.counter.step.stepcounter.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.counter.step.stepcounter.AppExecutors;
import ru.counter.step.stepcounter.db.MilestoneDao;
import ru.counter.step.stepcounter.db.TrackDao;
import ru.counter.step.stepcounter.repository.AppSettingsRepository;
import ru.counter.step.stepcounter.repository.MilestoneRepository;
import ru.counter.step.stepcounter.repository.TrackRepository;
import ru.counter.step.stepcounter.repository.UserRepository;

@Module(includes = {AppModule.class})
public class RepositoryModule {

    @Provides
    @Singleton
    AppSettingsRepository provideAppSettingsRepository() {
        return new AppSettingsRepository();
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository() {
        return new UserRepository();
    }

    @Provides
    @Singleton
    MilestoneRepository provideMilestoneRepository(MilestoneDao milestoneDao, AppExecutors appExecutors) {
        return new MilestoneRepository(milestoneDao, appExecutors);
    }

    @Provides
    @Singleton
    TrackRepository provideTrackRepository(TrackDao trackDao, AppExecutors appExecutors) {
        return new TrackRepository(trackDao, appExecutors);
    }
}
