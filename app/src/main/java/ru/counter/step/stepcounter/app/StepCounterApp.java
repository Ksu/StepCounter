package ru.counter.step.stepcounter.app;

import android.app.Application;

import ru.counter.step.stepcounter.BuildConfig;
import ru.counter.step.stepcounter.di.AppComponent;
import ru.counter.step.stepcounter.di.DaggerAppComponent;
import ru.counter.step.stepcounter.di.module.ContextModule;
import timber.log.Timber;

public class StepCounterApp extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        sAppComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .application(this)
                .build();
        sAppComponent.inject(this);
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
