package ru.counter.step.stepcounter.repository;

import javax.inject.Inject;

import ru.counter.step.stepcounter.util.UserUtil;

public class UserRepository {

    @Inject
    public UserRepository() {
    }

    public void setUserId(int userId) {
        UserUtil.setUserId(userId);
    }

    public int getUserId() {
        return UserUtil.getUserId();
    }
}
