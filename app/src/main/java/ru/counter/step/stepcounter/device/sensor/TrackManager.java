package ru.counter.step.stepcounter.device.sensor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.sensor.stepcounter.StepCounterManager;
import ru.counter.step.stepcounter.device.sensor.stepcounter.StepListener;
import ru.counter.step.stepcounter.eventbus.CounterDataEvent;
import ru.counter.step.stepcounter.eventbus.CounterErrorEvent;
import ru.counter.step.stepcounter.eventbus.CounterStopEvent;
import ru.counter.step.stepcounter.eventbus.MilestoneLatencyEvent;
import ru.counter.step.stepcounter.eventbus.NewMilestoneEvent;
import ru.counter.step.stepcounter.presentation.model.Milestone;
import ru.counter.step.stepcounter.presentation.model.Track;
import ru.counter.step.stepcounter.repository.AppSettingsRepository;
import ru.counter.step.stepcounter.repository.MilestoneRepository;
import ru.counter.step.stepcounter.repository.TrackRepository;
import ru.counter.step.stepcounter.repository.UserRepository;

public class TrackManager {
    @Inject
    AppSettingsRepository settingsRepository;
    @Inject
    UserRepository userRepository;
    @Inject
    StepCounterManager stepCounterManager;
    @Inject
    TrackRepository trackRepository;
    @Inject
    MilestoneRepository milestoneRepository;

    private boolean counterActive;
    private long stepsBeforePause;
    private long totalTrackSteps;
    private long totalTrackTime;
    private Disposable timerSubscription;
    long userId;
    long trackId;
    int milestoneLatencySec;

    public TrackManager() {
        StepCounterApp.getAppComponent().inject(this);
        EventBus.getDefault().register(this);
        userId = userRepository.getUserId();
        milestoneLatencySec = settingsRepository.getHistoryLatency();
    }

    public boolean isCounterActive() {
        return counterActive;
    }

    public void startTrack() {
        counterActive = true;
        saveTrackStart();
    }

    public void resumeTrack() {
        counterActive = true;
        readLastTrack();
    }

    private void onTrackReady() {
        startTimer();
        stepCounterManager.registerStepListener(getStepListener());
    }

    private StepListener getStepListener() {
        return new StepListener() {
            @Override
            public void onStepsDetected(int totalSteps) {
                totalTrackSteps = stepsBeforePause + totalSteps;
            }

            @Override
            public void onError(String message) {
                stopTrack();
                postErrorEvent(message);
            }
        };
    }

    private void startTimer() {
        timerSubscription = Observable.interval(1, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((tick -> {
                    totalTrackTime += 1;
                    postCounterDataEvent();
                    if (totalTrackTime % milestoneLatencySec == 0) {
                        saveCurrentProgress();
                    }
                }));
    }

    private void saveTrackStart() {
        Disposable disposable = Single.fromCallable(() -> {
            Track track = new Track(userId);
            return trackRepository.insertTrack(track);
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(id -> {
                    trackId = id;
                    onTrackReady();
                }, error -> {
                    error.printStackTrace();
                    postErrorEvent("Не удалось начать обход - ошибка записи в БД");
                    trackId = -1;
                    stopTrack();
                });
    }

    private void readLastTrack() {
        Disposable disposable = trackRepository.getLastTrackByUserId(543)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(track -> {
                    trackId = track.getId();
                    stepsBeforePause = track.getTotalTrackSteps();
                    totalTrackTime = track.getTotalTrackTimeSec();
                    onTrackReady();
                }, exception -> {
                    exception.printStackTrace();
                    saveTrackStart();
                });
    }

    private void saveCurrentProgress() {
        Milestone milestone = new Milestone(userId, trackId, totalTrackSteps, totalTrackTime);
        postNewMilestoneEvent(milestone);
        saveMilestone(milestone);
        updateTrackState(trackId, totalTrackSteps, totalTrackTime);
    }

    private void saveMilestone(Milestone milestone) {
        Completable.fromAction(() -> {
            milestoneRepository.insertMilestone(milestone);
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                postErrorEvent("Ошибка записи в БД");
            }
        });
    }

    private void updateTrackState(long trackId, long totalTrackSteps, long totalTrackTime) {
        Completable.fromAction(() -> {
            trackRepository.updateTrack(trackId, totalTrackSteps, totalTrackTime);
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                postErrorEvent("Ошибка записи в БД");
            }
        });
    }

    public void stopTrack() {
        stepCounterManager.unregisterStepListener();
        //исключаем состояния, когда нет id и когда такая запись уже существует
        if ((trackId != -1) && ((totalTrackTime % milestoneLatencySec != 0))) {
            saveCurrentProgress();
        }
        clearTrackData();
        counterActive = false;
        postStopEvent();
    }

    private void clearTrackData() {
        stepsBeforePause = 0;
        totalTrackSteps = 0;
        trackId = -1;
        stopTimer();
    }

    private void stopTimer() {
        totalTrackTime = 0;
        timerSubscription.dispose();
    }

    private void postCounterDataEvent() {
        EventBus.getDefault().post(new CounterDataEvent(totalTrackTime, totalTrackSteps));
    }

    private void postNewMilestoneEvent(Milestone milestone) {
        EventBus.getDefault().post(new NewMilestoneEvent(milestone));
    }

    private void postStopEvent() {
        EventBus.getDefault().post(new CounterStopEvent(false));
    }

    private void postErrorEvent(String message) {
        EventBus.getDefault().post(new CounterErrorEvent(message));
    }

    @Subscribe
    public void onMilestoneLatencyChanged(MilestoneLatencyEvent event) {
        milestoneLatencySec = event.newLatencySec;
    }

    public void cleanup() {
        EventBus.getDefault().unregister(this);
        timerSubscription.dispose();
        stepCounterManager.unregisterStepListener();
    }
}
