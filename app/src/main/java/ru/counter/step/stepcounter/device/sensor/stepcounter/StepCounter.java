package ru.counter.step.stepcounter.device.sensor.stepcounter;

public interface StepCounter {
    void registerStepListener(StepListener listener);

    void unregisterStepListener();
}
