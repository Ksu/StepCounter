package ru.counter.step.stepcounter.presentation.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "milestone")
public class Milestone {
    @SerializedName("id")
    @NonNull
    @Expose
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @SerializedName("userId")
    @Expose
    @ColumnInfo(name = "userId")
    private long userId;

    @SerializedName("trackId")
    @Expose
    @ColumnInfo(name = "trackId")
    private long trackId;

    @SerializedName("trackSteps")
    @Expose
    @ColumnInfo(name = "trackSteps")
    private long trackSteps;

    @SerializedName("trackTimeSec")
    @Expose
    @ColumnInfo(name = "trackTimeSec")
    private long trackTimeSec;

    public Milestone(long userId, long trackId, long trackSteps, long trackTimeSec) {
        this.userId = userId;
        this.trackId = trackId;
        this.trackSteps = trackSteps;
        this.trackTimeSec = trackTimeSec;
    }

    @NonNull
    public long getId() {
        return id;
    }

    public void setId(@NonNull long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getTrackId() {
        return trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }

    public long getTrackSteps() {
        return trackSteps;
    }

    public void setTrackSteps(long trackSteps) {
        this.trackSteps = trackSteps;
    }

    public long getTrackTimeSec() {
        return trackTimeSec;
    }

    public void setTrackTimeSec(long trackTimeSec) {
        this.trackTimeSec = trackTimeSec;
    }
}
