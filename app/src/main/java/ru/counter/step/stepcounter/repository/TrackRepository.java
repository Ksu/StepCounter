package ru.counter.step.stepcounter.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Maybe;
import io.reactivex.Single;
import ru.counter.step.stepcounter.AppExecutors;
import ru.counter.step.stepcounter.db.TrackDao;
import ru.counter.step.stepcounter.presentation.model.Track;

@Singleton
public class TrackRepository {
    private final TrackDao trackDao;
    private final AppExecutors appExecutors;

    @Inject
    public TrackRepository(TrackDao trackDao, AppExecutors appExecutors) {
        this.trackDao = trackDao;
        this.appExecutors = appExecutors;
    }

    public long insertTrack(Track track) {
        return trackDao.insertTrack(track);
    }

    public Single<List<Track>> getTracksByUserId(long userId) {
        return trackDao.getTracksByUserId(userId);
    }

    public Single<Track> getLastTrackByUserId(long userId) {
        return trackDao.getLastTrackByUserId(userId);
    }

    public void updateTrack(long trackId, long totalTrackSteps, long totalTrackTime) {
        trackDao.updateTrack(trackId, totalTrackSteps, totalTrackTime);
    }

    public void clear() {
        appExecutors.diskIO().execute(trackDao::deleteAll);
    }

}
