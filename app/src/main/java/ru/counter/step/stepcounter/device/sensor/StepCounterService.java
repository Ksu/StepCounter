package ru.counter.step.stepcounter.device.sensor;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import javax.inject.Inject;

import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.notification.NotificationFactory;

public class StepCounterService extends Service {
    public static final String ACTION_JUST_CHECK = "ACTION_JUST_CHECK";
    public static final String ACTION_START_TRACK = "ACTION_START_TRACK";
    public static final String ACTION_STOP_TRACK = "ACTION_STOP_TRACK";
    public static final String ACTION_RESUME_LAST_TRACK = "ACTION_RESUME_LAST_TRACK";

    @Inject
    TrackManager trackManager;
    @Inject
    NotificationFactory notificationFactory;

    private final IBinder mBinder = new StepCounterBinder();

    public class StepCounterBinder extends Binder {
        public StepCounterService getService() {
            return StepCounterService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        StepCounterApp.getAppComponent().inject(this);
        notificationFactory.createStepCounterNotificationChannel();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //the app must call that service's startForeground() method
            //within five seconds after the service is created
            //or the Service throws a "Context.startForegroundService()
            //did not then call Service.startForeground()" error
            Notification notification = notificationFactory.getNotification(NotificationFactory.Type.STEP_COUNTER);
            startForeground(1, notification);
        }

        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_JUST_CHECK:
                        break;
                    case ACTION_START_TRACK:
                        startTrack();
                        break;
                    case ACTION_RESUME_LAST_TRACK:
                        resumeTrack();
                        break;
                    case ACTION_STOP_TRACK:
                        stopTrack();
                        break;
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public boolean isCounterActive() {
        return trackManager.isCounterActive();
    }

    public void startTrack() {
        trackManager.startTrack();
        Notification notification = notificationFactory.getNotification(NotificationFactory.Type.STEP_COUNTER_WITH_STOP_ACTION);
        startForeground(1, notification);// если уже запущен - просто заменит уведомление
    }

    public void resumeTrack() {
        trackManager.resumeTrack();
        Notification notification = notificationFactory.getNotification(NotificationFactory.Type.STEP_COUNTER_WITH_STOP_ACTION);
        startForeground(1, notification);// если уже запущен - просто заменит уведомление
    }

    public void stopTrack() {
        trackManager.stopTrack();
        stopForeground(true);
        stopSelf();
    }

    @Override
    public void onDestroy() {
        trackManager.cleanup();
        super.onDestroy();
    }
}
