package ru.counter.step.stepcounter.util;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class FormatUtil {

    public static String getFormattedTimerValue(long currentTrackTimeMS) {
        return String.format(Locale.getDefault(), "%02d : %02d",
                TimeUnit.MILLISECONDS.toMinutes(currentTrackTimeMS),
                TimeUnit.MILLISECONDS.toSeconds(currentTrackTimeMS) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentTrackTimeMS))
        );
    }
}
