package ru.counter.step.stepcounter.device.sensor.stepcounter;

import android.hardware.Sensor;
import android.hardware.SensorManager;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.sensor.stepcounter.legacydevice.LegacyStepDetector;
import ru.counter.step.stepcounter.util.RxSensorUtil;
import ru.counter.step.stepcounter.util.Utils;
import rx.Subscription;

import static android.hardware.Sensor.TYPE_ACCELEROMETER;

public class LegacyStepCounter implements StepCounter, LegacyStepDetector.StepDetectorListener {

    public static final int SENSOR_TYPE = TYPE_ACCELEROMETER;
    private static final int SENSOR_DELAY = SensorManager.SENSOR_DELAY_FASTEST; // sampling period, microseconds

    @Nonnull
    private Sensor sensor;
    @Nonnull
    private SensorManager sensorManager;
    private StepListener stepListener;
    private Subscription counterSubscription;

    public int totalTrackSteps;

    @Inject
    LegacyStepDetector legacyStepDetector;


    public LegacyStepCounter(@Nonnull SensorManager sensorManager, @Nonnull Sensor sensor) {
        StepCounterApp.getAppComponent().inject(this);
        this.sensorManager = sensorManager;
        this.sensor = sensor;
    }

    @Override
    public void stepDetected(long timeNs) {
        totalTrackSteps++;
        if (stepListener != null) {
            stepListener.onStepsDetected(totalTrackSteps);
        }
    }

    public void registerStepListener(StepListener listener) {
        stepListener = listener;
        totalTrackSteps = 0;
        legacyStepDetector.registerListener(this);

        counterSubscription = RxSensorUtil.observeSensorChanged(sensorManager, sensor, SENSOR_DELAY)
                .compose(Utils.applySchedulers())
                .subscribe(event -> {

                    if (event.sensor.getType() == TYPE_ACCELEROMETER) {
                        legacyStepDetector.updateAccel(
                                event.timestamp, event.values[0], event.values[1], event.values[2]);
                    }
                }, exception -> {
                    exception.printStackTrace();
                    if (stepListener != null) {
                        stepListener.onError("Ошибка получения данных");
                    }
                });
    }

    @Override
    public void unregisterStepListener() {
        stepListener = null;
        counterSubscription.unsubscribe();
        totalTrackSteps = 0;
    }
}
