package ru.counter.step.stepcounter.util;


public class HistoryLatencyUtil {

    private static final String HISTORY_LATENCY = "latency";
    private static final int DEFAULT_MILESTONE_LATENCY_SEC = 5;


    public static int getHistoryLatency() {
        return PrefUtils.getPrefs().getInt(HISTORY_LATENCY, DEFAULT_MILESTONE_LATENCY_SEC);
    }

    public static void setHistoryLatency(int latency) {
        PrefUtils.getEditor().putInt(HISTORY_LATENCY, latency).commit();
    }
}
