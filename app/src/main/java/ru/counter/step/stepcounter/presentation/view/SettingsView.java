package ru.counter.step.stepcounter.presentation.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface SettingsView extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void invalidInput();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void validInput();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setMinutesSelection(int minites);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setSecondsSelection(int seconds);

}
