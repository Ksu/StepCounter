package ru.counter.step.stepcounter.device.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import javax.inject.Inject;

import ru.counter.step.stepcounter.R;
import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.sensor.StepCounterService;
import ru.counter.step.stepcounter.ui.activity.MainActivity;

public class NotificationFactory {

    public static final String STEP_COUNTER_CHANNEL_ID = "STEP_COUNTER_CHANNEL";

    public enum Type {
        STEP_COUNTER("STEP_COUNTER"),
        STEP_COUNTER_WITH_STOP_ACTION("STEP_COUNTER_WITH_STOP_ACTION");

        private final String text;
        private Type(final String text) {
            this.text = text;
        }
        @Override
        public String toString() {
            return text;
        }
    }

    @Inject
    Context context;

    public NotificationFactory() {
        StepCounterApp.getAppComponent().inject(this);
    }

    public Notification getNotification(Type type) {
        switch (type) {
            case STEP_COUNTER:
                return getStepCounterNotification();
            case STEP_COUNTER_WITH_STOP_ACTION:
                return getStepCounterNotificationWithStopAction();
            default:
                return getStepCounterNotification();
        }
    }

    private Notification getStepCounterNotification() {
        NotificationCompat.Builder builder = getStepCounterNotificationBuilder();
        return builder.build();
    }

    private Notification getStepCounterNotificationWithStopAction() {
        NotificationCompat.Builder builder = getStepCounterNotificationBuilder();
        // Add Pause button intent in notification.
        NotificationCompat.Action stopAction = getStopStepCounterAction();
        builder.addAction(stopAction);
        return builder.build();
    }

    private NotificationCompat.Builder getStepCounterNotificationBuilder() {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationFactory.STEP_COUNTER_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.counter_notification_big_text_title))
                .setContentText(context.getString(R.string.counter_notification_big_text))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        return builder;
    }

    private NotificationCompat.Action getStopStepCounterAction() {
        Intent stopCounterIntent = new Intent(context, StepCounterService.class);
        stopCounterIntent.setAction(StepCounterService.ACTION_STOP_TRACK);
        PendingIntent pendingStopIntent = PendingIntent.getService(context, 0, stopCounterIntent, 0);
        return new NotificationCompat.Action(android.R.drawable.ic_menu_close_clear_cancel, "Завершить обход", pendingStopIntent);
    }

    public void createStepCounterNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.counter_notification_channel_name);
            String description = context.getString(R.string.counter_notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(STEP_COUNTER_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}
