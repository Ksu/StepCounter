package ru.counter.step.stepcounter.ui.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.counter.step.stepcounter.R;
import ru.counter.step.stepcounter.presentation.presenter.SettingsPresenter;
import ru.counter.step.stepcounter.presentation.view.SettingsView;

public class SettingsActivity extends MvpAppCompatActivity implements SettingsView {

    @InjectPresenter
    SettingsPresenter mSettingsPresenter;

    @BindView(R.id.pickers_header)
    TextView mPickersHeaderView;
    @BindView(R.id.minutes_picker)
    NumberPicker mMinitesPickerView;
    @BindView(R.id.seconds_picker)
    NumberPicker mSecondsPickerView;
    @BindView(R.id.save_changes)
    Button mSaveChangesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mMinitesPickerView.setMinValue(0);
        mMinitesPickerView.setMaxValue(10);

        mSecondsPickerView.setMinValue(0);
        mSecondsPickerView.setMaxValue(59);

        mMinitesPickerView.setOnValueChangedListener(getMinitesListener());
        mSecondsPickerView.setOnValueChangedListener(getSecondsListener());
    }

    private NumberPicker.OnValueChangeListener getMinitesListener () {
        return (numberPicker, i, i1) -> mSettingsPresenter.minitesChanged(numberPicker.getValue());
    }

    private NumberPicker.OnValueChangeListener getSecondsListener () {
        return (numberPicker, i, i1) -> mSettingsPresenter.secondsChanged(numberPicker.getValue());
    }

    @OnClick(R.id.save_changes)
    public void saveChanges(View view) {
        mSettingsPresenter.saveChanges();
    }

    @Override
    public void invalidInput() {
        disableButton(mSaveChangesView);
    }

    @Override
    public void validInput() {
        enableButton(mSaveChangesView);
    }

    @Override
    public void setMinutesSelection(int minutes) {
        mMinitesPickerView.setValue(minutes);
    }

    @Override
    public void setSecondsSelection(int seconds) {
        mSecondsPickerView.setValue(seconds);
    }

    private void enableButton(Button button) {
        button.setEnabled(true);
        button.setTextColor(ContextCompat.getColor(this, R.color.colorEnabledButton));
    }

    private void disableButton(Button button) {
        button.setEnabled(false);
        button.setTextColor(ContextCompat.getColor(this, R.color.colorDisabledButton));
    }
}
