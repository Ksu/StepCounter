package ru.counter.step.stepcounter.eventbus;

public class CounterStopEvent {

    public final boolean stoppedFromNotification;

    public CounterStopEvent(boolean stoppedFromNotification) {
        this.stoppedFromNotification = stoppedFromNotification;
    }
}
