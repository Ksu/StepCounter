package ru.counter.step.stepcounter.presentation.presenter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;

import com.arellomobile.mvp.InjectViewState;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.device.sensor.stepcounter.StepCounterManager;
import ru.counter.step.stepcounter.device.sensor.StepCounterService;
import ru.counter.step.stepcounter.eventbus.CounterDataEvent;
import ru.counter.step.stepcounter.eventbus.CounterErrorEvent;
import ru.counter.step.stepcounter.eventbus.CounterStopEvent;
import ru.counter.step.stepcounter.eventbus.NewMilestoneEvent;
import ru.counter.step.stepcounter.presentation.model.Milestone;
import ru.counter.step.stepcounter.presentation.view.StepCounterView;
import ru.counter.step.stepcounter.repository.MilestoneRepository;
import ru.counter.step.stepcounter.repository.TrackRepository;
import ru.counter.step.stepcounter.repository.UserRepository;
import ru.counter.step.stepcounter.ui.activity.SettingsActivity;

@InjectViewState
public class CounterPresenter extends BasePresenter<StepCounterView> {
    @Inject
    Context context;
    @Inject
    TrackRepository trackRepository;
    @Inject
    MilestoneRepository milestoneRepository;
    @Inject
    UserRepository userRepository;
    @Inject
    StepCounterManager stepCounterManager;
    private StepCounterService stepCounterService;
    private ServiceConnection serviceConnection;
    private boolean serviceBound;
    private boolean counterActive;

    public CounterPresenter() {
        StepCounterApp.getAppComponent().inject(this);
        serviceConnection = getServiceConnection();
    }

    @Override
    protected void onFirstViewAttach() {
        getViewState().showWarning(stepCounterManager.getCompatibilityInfo());
        if (stepCounterManager.deviceIsCompatible()) {
            loadHistory();
            EventBus.getDefault().register(this);
            startStepCounterService(StepCounterService.ACTION_JUST_CHECK);
        }
    }

    private void startStepCounterService(String action) {
        Intent intent = new Intent(context, StepCounterService.class);
        intent.setAction(action);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //the app must call that service's startForeground() method
            //within five seconds after the service is created
            //or the Service throws a "Context.startForegroundService()
            //did not then call Service.startForeground()" error
            try {
                context.startForegroundService(intent);
                context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            context.startService(intent);
            context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection getServiceConnection() {
        return new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                StepCounterService.StepCounterBinder binder = (StepCounterService.StepCounterBinder) service;
                stepCounterService = binder.getService();
                onStepCounterServiceBound();
            }

            // Called when the connection with the service disconnects unexpectedly
            public void onServiceDisconnected(ComponentName className) {
                serviceBound = false;
                startStepCounterService(StepCounterService.ACTION_JUST_CHECK);
            }
        };
    }

    private void onStepCounterServiceBound() {
        serviceBound = true;
        if (stepCounterService.isCounterActive()) {
            counterActive = true;
            getViewState().counterActive();

        } else {
            counterActive = false;
            getViewState().counterInactive();
            checkLastTrackExists();
        }
    }

    public void turnOnCounter() {
        counterActive = true;
        getViewState().counterActive();
        if (serviceBound) {
            stepCounterService.startTrack();
        } else {
            startStepCounterService(StepCounterService.ACTION_START_TRACK);
        }
    }

    public void resumeCounter() {
        counterActive = true;
        getViewState().counterActive();
        if (serviceBound) {
            stepCounterService.resumeTrack();
        } else {
            startStepCounterService(StepCounterService.ACTION_RESUME_LAST_TRACK);
        }
    }

    public void turnOffCounter() {
        counterActive = false;
        getViewState().counterInactive();
        stepCounterService.stopTrack();
    }

    @Subscribe
    public void onCounterDataEvent(CounterDataEvent event) {
        getViewState().showCurrentTimer(event.totalTrackTime * 1000);
        getViewState().showCurrentSteps(String.valueOf(event.totalTrackSteps));
        getViewState().counterActive();
    }

    @Subscribe
    public void onNewMilestoneEvent(NewMilestoneEvent event) {
        getViewState().addMilestoneRecord(event.milestone.getTrackId(), event.milestone.getTrackSteps(), (event.milestone.getTrackTimeSec() * 1000));
    }

    @Subscribe
    public void onCounterStopEvent(CounterStopEvent event) {
        counterActive = false;
        getViewState().counterInactive();
        getViewState().allowResume();
    }

    @Subscribe
    public void onCounterErrorEvent(CounterErrorEvent event) {
        getViewState().showError(event.message);
    }

    private void loadHistory() {
        long userId = userRepository.getUserId();
        Disposable disposable = milestoneRepository.getMilestonesByUserId(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(milestones -> {
                    for (Milestone milestone : milestones) {
                        getViewState().addMilestoneRecord(milestone.getTrackId(), milestone.getTrackSteps(), (milestone.getTrackTimeSec() * 1000));
                    }
                }, exception -> {
                    exception.printStackTrace();
                    getViewState().showHistoryLoadingError("Ошибка чтения БД");
                });
        disposeOnDestroy(disposable);
    }


    private void checkLastTrackExists() {
        long userId = userRepository.getUserId();
        Disposable disposable = trackRepository.getLastTrackByUserId(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(track -> {
                    if (!counterActive) {
                        getViewState().allowResume();
                    }
                }, exception -> {
                    //no track to continue, ok
                });
    }

    public void changeHistoryLatency() {
        Intent intent = new Intent(context, SettingsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
