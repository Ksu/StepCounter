package ru.counter.step.stepcounter.eventbus;

import ru.counter.step.stepcounter.presentation.model.Milestone;

public class NewMilestoneEvent {

    public final Milestone milestone;

    public NewMilestoneEvent(Milestone milestone) {
        this.milestone = milestone;
    }
}
