package ru.counter.step.stepcounter.eventbus;

public class CounterErrorEvent {

    public final String message;

    public CounterErrorEvent(String message) {
        this.message = message;
    }
}
