package ru.counter.step.stepcounter.eventbus;

public class CounterDataEvent {

    public final long totalTrackTime;
    public final long totalTrackSteps;

    public CounterDataEvent(long totalTrackTime, long totalTrackSteps) {
        this.totalTrackTime = totalTrackTime;
        this.totalTrackSteps = totalTrackSteps;
    }
}
