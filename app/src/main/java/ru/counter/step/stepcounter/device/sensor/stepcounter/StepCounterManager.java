package ru.counter.step.stepcounter.device.sensor.stepcounter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import javax.annotation.Nullable;
import javax.inject.Inject;

import ru.counter.step.stepcounter.R;
import ru.counter.step.stepcounter.app.StepCounterApp;

import static android.hardware.Sensor.TYPE_ACCELEROMETER;
import static android.hardware.Sensor.TYPE_STEP_COUNTER;

public class StepCounterManager {

    @Inject
    Context context;
    @Inject
    PackageManager packageManager;
    @Inject
    @Nullable
    SensorManager sensorManager;

    StepCounter stepCounter;

    public StepCounterManager() {
        StepCounterApp.getAppComponent().inject(this);
    }

    public void registerStepListener(StepListener listener) {
        if (stepCounterSupported()) {
            Sensor sensor = sensorManager.getDefaultSensor(DefaultStepCounter.SENSOR_TYPE); // sensorManager не null, проверено в stepCounterSupported()
            stepCounter = new DefaultStepCounter(sensorManager, sensor);
            stepCounter.registerStepListener(listener);

        } else if (accelerometerSupported()) {
            Sensor sensor = sensorManager.getDefaultSensor(LegacyStepCounter.SENSOR_TYPE); // sensorManager не null, проверено в accelerometerSupported()
            stepCounter = new LegacyStepCounter(sensorManager, sensor);
            stepCounter.registerStepListener(listener);
        }
    }

    public void unregisterStepListener() {
        if (stepCounter != null) {
            stepCounter.unregisterStepListener();
        }
    }

    public boolean deviceIsCompatible() {
        return (stepCounterSupported() || accelerometerSupported());
    }


    public String getCompatibilityInfo() {
        if (!deviceIsCompatible()) {
            return context.getString(R.string.warning_no_sensors);
        } else if (stepCounterSupported()) {
            return "";
        } else {
            return context.getString(R.string.warning_no_step_counter);
        }
    }


    /**
     * Returns true if this device is supported. It needs to be running Android KitKat (4.4) or
     * higher and has a step counter and step detector sensor.
     * This check is useful when an app provides an alternative implementation or different
     * functionality if the step sensors are not available or this code runs on a platform version
     * below Android KitKat. If this functionality is required, then the minSDK parameter should
     * be specified appropriately in the AndroidManifest.
     */
    public boolean stepCounterSupported() {
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        return currentApiVersion >= android.os.Build.VERSION_CODES.KITKAT
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)
                && (sensorManager != null)
                && (sensorManager.getDefaultSensor(TYPE_STEP_COUNTER) != null);
    }

    public boolean accelerometerSupported() {
        return (sensorManager != null)
                && (sensorManager.getDefaultSensor(TYPE_ACCELEROMETER) != null);
    }
}
