package ru.counter.step.stepcounter.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.counter.step.stepcounter.presentation.model.Milestone;
import ru.counter.step.stepcounter.presentation.model.Track;

@Database(entities = {Track.class, Milestone.class}, version = 1, exportSchema = false)
public abstract class StepCounterDB extends RoomDatabase {
    public abstract TrackDao trackDao();

    public abstract MilestoneDao milestoneDao();
}