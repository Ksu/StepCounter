package ru.counter.step.stepcounter.util;

import android.content.Context;
import android.content.SharedPreferences;

import ru.counter.step.stepcounter.app.StepCounterApp;

public class PrefUtils {
    private static final String PREF_NAME = "step_counter";

    public static SharedPreferences getPrefs() {
        return StepCounterApp.getAppComponent().getContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }
}