package ru.counter.step.stepcounter.presentation.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface StepCounterView extends MvpView {

    void addMilestoneRecord(long trackId, long totalTrackSteps, long totalTrackTimeMS);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showCurrentSteps(String currentTrackSteps);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showCurrentTimer(long currentTrackTimeMS);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showError(String message);

    void showHistoryLoadingError(String message);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showWarning(String message);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void counterActive();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void counterInactive();


//    @StateStrategyType(AddToEndSingleStrategy.class)
//    void activateCounter(boolean active);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void allowResume();
}
