package ru.counter.step.stepcounter.util;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import rx.AsyncEmitter;
import rx.Observable;

public class RxSensorUtil {

    public static Observable<SensorEvent> observeSensorChanged(SensorManager sensorManager, Sensor sensor, int samplingPeriodUs, int maxReportLatencyUs) {
        return Observable.fromEmitter(sensorEventAsyncEmitter -> {
            final SensorEventListener sensorListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    sensorEventAsyncEmitter.onNext(sensorEvent);
                }

                @Override
                public void onAccuracyChanged(Sensor originSensor, int i) {
                    // do nothing
                }
            };
            sensorEventAsyncEmitter.setCancellation(() ->
                    sensorManager.unregisterListener(sensorListener, sensor));
            sensorManager.registerListener(sensorListener, sensor,
                    samplingPeriodUs, maxReportLatencyUs);
        }, AsyncEmitter.BackpressureMode.LATEST);
    }

    public static Observable<SensorEvent> observeSensorChanged(SensorManager sensorManager, Sensor sensor, int samplingPeriodUs) {
        return Observable.fromEmitter(sensorEventAsyncEmitter -> {
            final SensorEventListener sensorListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    sensorEventAsyncEmitter.onNext(sensorEvent);
                }

                @Override
                public void onAccuracyChanged(Sensor originSensor, int i) {
                    // do nothing
                }
            };
            sensorEventAsyncEmitter.setCancellation(() ->
                    sensorManager.unregisterListener(sensorListener, sensor));
            sensorManager.registerListener(sensorListener, sensor,
                    samplingPeriodUs);
        }, AsyncEmitter.BackpressureMode.LATEST);
    }
}
