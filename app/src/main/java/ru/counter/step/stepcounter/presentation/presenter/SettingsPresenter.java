package ru.counter.step.stepcounter.presentation.presenter;

import android.content.Context;
import android.widget.Toast;

import com.arellomobile.mvp.InjectViewState;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import ru.counter.step.stepcounter.app.StepCounterApp;
import ru.counter.step.stepcounter.eventbus.MilestoneLatencyEvent;
import ru.counter.step.stepcounter.presentation.view.SettingsView;
import ru.counter.step.stepcounter.repository.AppSettingsRepository;

@InjectViewState
public class SettingsPresenter extends BasePresenter<SettingsView> {
    @Inject
    Context context;
    @Inject
    AppSettingsRepository settingsRepository;

    private int minites;
    private int seconds;

    public SettingsPresenter() {
        StepCounterApp.getAppComponent().inject(this);
    }

    @Override
    public void onFirstViewAttach() {
        int latency = settingsRepository.getHistoryLatency();
        minites = (int) (latency / 60);
        seconds = (int) (latency - (minites * 60));

        getViewState().setMinutesSelection(minites);
        getViewState().setSecondsSelection(seconds);
    }

    public void minitesChanged(int value) {
        minites = value;
        getViewState().setMinutesSelection(minites);
        validateChanges();

    }

    public void secondsChanged(int value) {
        seconds = value;
        getViewState().setSecondsSelection(seconds);
        validateChanges();
    }

    private void validateChanges() {
        if ((minites == 0) && (seconds == 0)) {
            getViewState().invalidInput();
        } else {
            getViewState().validInput();
        }
    }

    public void saveChanges() {
        int latency = minites * 60 + seconds;
        settingsRepository.setHistoryLatency(latency);
        EventBus.getDefault().post(new MilestoneLatencyEvent(latency));
        Toast.makeText(context, "Изменения сохранены", Toast.LENGTH_SHORT).show();
    }
}
